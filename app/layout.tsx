import "@/styles/globals.css"

import Script from "next/script"
import { Metadata, Viewport } from "next"

import { lato } from "@/app/ui/fonts"
import Navbar from "@/components/navbarArea/Navbar"

import { Providers } from "./providers"

export const metadata: Metadata = {
  title: "LINCS Dashboard",
  description:
    "The LINCS dashboard displaying key community, development, and publication statistics.",
  authors: [{ name: "LINCS Dev Team" }],
  keywords: [
    "LINCS",
    "LINCS Dashboard",
    "LINCS Community",
    "LINCS Stats",
    "LINCS Information",
    "LINCS Info",
  ],
  openGraph: {
    title: "Your central hub for all stats regarding LINCS",
    description: "The LINCS dashboard displaying key community, development, and publication statistics.",
    images: "",
    siteName: "LINCS Dashboard",
    type: "website",
  },
}

export const viewport: Viewport = {
  initialScale: 1,
  width: "device-width",
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html suppressHydrationWarning lang="en">
      <head>
        <title>LINCS Dashboard</title>
        {/* <!-- Matomo --> */}
        <Script type="text/javascript" id="matomo-tracking">
          {`
        var _paq = window._paq = window._paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
          var u="//matomo.lincsproject.ca/";
          _paq.push(['setTrackerUrl', u+'matomo.php']);
          _paq.push(['setSiteId', '7']);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();`}
        </Script>
        {/* <!-- End Matomo Code --> */}
      </head>
      <body className={`antialiased ${lato.className}`}>
        <Navbar />
        <Providers
          themeProps={{
            attribute: "class",
            defaultTheme: "light",
          }}
        >
          {children}
        </Providers>
      </body>
    </html>
  )
}
