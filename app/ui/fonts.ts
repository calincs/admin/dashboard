// Keep track of fonts that will be used throughout the app
import { Nunito, Lato } from "next/font/google"

export const nunito = Nunito({ subsets: ["latin"] })
export const lato = Lato({ subsets: ["latin"], weight: ["400", "700"] })
