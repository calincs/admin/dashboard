"use client"

import { today, getLocalTimeZone, CalendarDate } from "@internationalized/date"
import { createContext, SetStateAction, Dispatch } from "react"

import { getActivityThreshold } from "./definitions"

const defaultDate = getActivityThreshold()
const startDate = new CalendarDate(
  defaultDate[0],
  defaultDate[1],
  defaultDate[2]
)
const endDate = today(getLocalTimeZone())

interface DateRangeFormat {
  start: CalendarDate
  end: CalendarDate
  error: boolean
  defaultStart: CalendarDate
  defaultEnd: CalendarDate
}
type DateRangeContext = {
  dateRange: DateRangeFormat
  setDateRange: Dispatch<SetStateAction<DateRangeFormat>>
}

export const defaultFormattedDate: DateRangeFormat = {
  start: startDate,
  end: endDate,
  error: false,
  defaultStart: startDate,
  defaultEnd: endDate,
}
export const DateRangeFromContext = createContext<DateRangeContext>({
  dateRange: defaultFormattedDate,
  setDateRange: () => {},
})
