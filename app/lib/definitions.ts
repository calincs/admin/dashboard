// TYPES FOR CARDAREAS
export type cardQueries =
  | "numActiveRepos"
  | "numContributions"
  | "numOpenedIssues"
  | "numClosedIssues"
  | "numUsers"
  | "numActiveUsers"
  | "numWorkshops"
  | "numNewUsers"
export type cardCategories = "keycloak" | "gitlab" | "workshop"

// CONSTANTS FOR STACKED BAR CHART COLOURS
export const STACKED_PUBPRES_BAR_CHART_COLOURS = {
  "2020": "rgb(255, 99, 132)",
  "2021": "rgb(75, 192, 192)",
  "2022": "rgb(53, 162, 235)",
  "2023": "rgb(255, 150, 79)",
  "2024": "rgb(159, 185, 191)",
  other: "rgb(177, 156, 217)",
}

export const STACKED_MATOMO_BAR_CHART_COLOURS = {
  "LINCS Portal": "rgb(75, 192, 192)",
  "Context Explorer (Browser Plugin)": "rgb(255, 99, 132)",
  "LINCS Vocabulary Browser": "rgb(53, 162, 235)",
  ResearchSpace: "rgb(255, 150, 79)",
}

// CONSTANTS FOR MATOMO QUERIES
export const SITEIDS = [6, 5, 4, 3]

// CONSTANTS FOR SPARQL QUERIES
export const SPARQL_ENDPOINT = "https://fuseki.lincsproject.ca/lincs"
export const LINKMAP = {
  AdArchive: "adarchive",
  "Anthologia graeca": "anthologia-graeca",
  Ethnomusicology: "ethnomusicology",
  Heresies: "heresies-project",
  HistSex: "histsex",
  MoEml: "moeml",
  Orlando: "orlando",
  "USask Art Collection": "usask-art",
  "Yellow Nineties": "yellow-nineties",
}

// CONSTANTS FOR SVG ICONS
export const METRICS_ICON_HEIGHT_WIDTH = "32px"

// CONSTANTS AND FUNCTIONS FOR ACTIVITY THRESHOLDS
export const DEFAULT_MONTH_THRESHOLD = 6
export const MAX_NUM_USERS = 1000
export function getActivityThreshold() {
  const now = new Date()
  const day = now.getDate()
  let month = now.getMonth() + 1
  let year = now.getFullYear()

  if (month - DEFAULT_MONTH_THRESHOLD <= 0) {
    year -= 1
    month = 12 - (DEFAULT_MONTH_THRESHOLD - month)
  } else {
    month = month - DEFAULT_MONTH_THRESHOLD
  }

  return [year, month, day]
}
export function getActivityThresholdString() {
  const threshold = getActivityThreshold()

  const date = new Date(threshold[0], threshold[1] - 1, threshold[2])
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
  }

  return date.toLocaleDateString("en-US", options)
}

export function getDateString(date: number[]) {
  const dateStr = new Date(date[0], date[1] - 1, date[2])
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
  }

  return dateStr.toLocaleDateString("en-US", options)
}

// function addDaySuffix(day: number) {
//   if (day > 3 && day < 21) return day + "th" // covers 4th - 20th
//   switch (day % 10) {
//     case 1:
//       return day + "st"
//     case 2:
//       return day + "nd"
//     case 3:
//       return day + "rd"
//     default:
//       return day + "th"
//   }
// }

export const YEARS = ["2021", "2022", "2023", "2024"]
export const MONTHS = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
]
