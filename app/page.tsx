"use client"

import { useState, useEffect } from "react"

import SideNav from "@/components/sidebarArea/SideNavbar"
import TriplesCountTable from "@/components/datasetTable/triplesCountTable"
import DevCardSection from "@/components/cardArea/DevCardSection"
import UserCardSection from "@/components/cardArea/UserCardSection"
import PubSumChart from "@/components/barchartArea/PubPresChart"
import UniqueVisitorsChart from "@/components/barchartArea/UniqueVisitorsChart"
import SectionHeader from "@/components/sectionHeaderArea/SectionHeader"
import {
  defaultFormattedDate,
  DateRangeFromContext,
} from "@/app/lib/dateRangeContext"

export default function Home() {
  const [dateRange, setDateRange] = useState(defaultFormattedDate)

  useEffect(() => {
    const { start, end } = dateRange

    if (
      start.year > end.year ||
      (start.year === end.year && start.month > end.month)
    ) {
      setDateRange({ ...dateRange, error: true })
    } else {
      setDateRange({ ...dateRange, error: false })
    }
  }, [dateRange.start, dateRange.end])

  return (
    <>
      <div className="flex flex-col p-10 gap-5 xl:grid xl:grid-cols-5">
        <div className="xl:col-span-1">
          <DateRangeFromContext.Provider
            value={{ dateRange: dateRange, setDateRange: setDateRange }}
          >
            <SideNav />
          </DateRangeFromContext.Provider>
        </div>
        <div className="xl:col-span-4">
          <div className="gap-5 mb-12 xl:grid xl:grid-cols-4 xl:mb-5 xl:">
            <div className="xl:col-span-3 xl:flex xl:flex-col xl:mb-0 mb-12">
              {/* DEVELOPMENT STATS */}
              <div>
                <SectionHeader
                  header="Development"
                  href="https://gitlab.com/calincs/admin/dashboard"
                  description="Fetched through GitLab API, our software repository platform."
                />
                <DateRangeFromContext.Provider
                  value={{
                    dateRange: defaultFormattedDate,
                    setDateRange: setDateRange,
                  }}
                >
                  <DevCardSection />
                </DateRangeFromContext.Provider>
              </div>
              {/* PUBLICATIONS AND PRESENTATIONS STATS */}
              <div>
                <SectionHeader
                  header="Publications and Presentations"
                  href="https://lincsproject.ca/docs/about-lincs/publications-and-presentations"
                  description="LINCS publications and presentations through the years."
                />
                <div className="flex flex-col xl:grid xl:grid-cols-2 gap-5">
                  <PubSumChart type="publication" />
                  <PubSumChart type="presentation" />
                </div>
              </div>
            </div>
            {/* TRIPLES TABLE */}
            <div className="flex flex-col">
              <SectionHeader
                header="Triples"
                description="Our LINCS Linked Open Data (LOD) database fetched through our LINCS API."
                href="https://portal.lincsproject.ca/docs/explore-lod/project-datasets/"
              />
              <TriplesCountTable />
            </div>
          </div>
          {/* RESOURCES AND COMMUNITY */}
          <div className="flex flex-row gap-4">
            <SectionHeader
              header="Resources and Community"
              description="Our wonderful LINCS Community stats fetched through Keycloak and Matomo services."
              href="https://portal.lincsproject.ca/docs/about-lincs/get-involved"
            />
          </div>
          <div className="flex flex-col gap-5 pb-12">
            <UserCardSection />
            <DateRangeFromContext.Provider
              value={{
                dateRange: dateRange,
                setDateRange: setDateRange,
              }}
            >
              <UniqueVisitorsChart />
            </DateRangeFromContext.Provider>
          </div>
        </div>
      </div>
    </>
  )
}
