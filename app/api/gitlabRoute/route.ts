import { NextResponse, NextRequest } from "next/server"

import { getActivityThreshold } from "@/app/lib/definitions"

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("query")
  const startDate = searchParams.get("start")
  const endDate = searchParams.get("end")
  const defaultStart = (searchParams.get("defaultStart") as String)
    .split("-")
    .map(Number)
  const calincsID = 7139154
  let response

  //NUMBER OF OPEN & CLOSED ISSUES
  // https://docs.gitlab.com/ee/api/issues_statistics.html#get-group-issues-statistics

  // ASKS IF THERE'S A TIME RANGE
  if (!startDate || !endDate) {
    throw "No start of end date in query"
  }

  if (query === "numOpenedIssues" || query === "numClosedIssues") {
    response = await fetch(
      `https://gitlab.com/api/v4/groups/${calincsID}/issues_statistics?created_after=${startDate}&created_before=${endDate}&private_token=${process.env.GITLAB_PERSONAL_TOKEN}`
    )
    if (response.ok) {
      const data = await response.json()

      if (query === "numOpenedIssues") {
        return NextResponse.json({
          result: data.statistics.counts.opened,
        })
      }

      return NextResponse.json({
        result: data.statistics.counts.closed,
      })
    }
  }

  //NUMBER OF ACTIVE REPOS
  else if (query === "numActiveRepos") {
    response = await fetchSubGroups(calincsID)

    if (response) {
      const groupIDs = await getGroupIDs({ calincsID, data: response })
      let activeRepoCount = 0

      for (const a in groupIDs) {
        const repoInGroup = await fetchReposPerGroup(groupIDs[a])

        if (repoInGroup) {
          for (const b in repoInGroup) {
            if (
              checkActivity(
                repoInGroup[b] as { [key: string]: any },
                defaultStart
              )
            ) {
              activeRepoCount += 1
            }
          }
        }
      }

      return NextResponse.json({ result: activeRepoCount })
    }
  }

  //NUMBER OF CONTRIBUTIONS
  else if (query === "numContributions") {
    let contributionCount = 0

    response = await fetchSubGroups(calincsID)

    if (!response) {
      throw "Can't fetch subgroups"
    }
    const groupIDs = await getGroupIDs({ calincsID, data: response })

    for (const a in groupIDs) {
      const repoInGroup = await fetchReposPerGroup(groupIDs[a])

      if (repoInGroup) {
        for (const b in repoInGroup) {
          if (
            checkActivity(
              repoInGroup[b] as { [key: string]: any },
              defaultStart
            )
          ) {
            const repoContributors = await fetchRepo(repoInGroup[b].id)

            if (repoContributors) {
              for (const c in repoContributors) {
                contributionCount += repoContributors[c].commits
              }
            }
          }
        }
      }
    }

    return NextResponse.json({ result: contributionCount })
  } else {
    throw "No query received"
  }

  throw "No data received from GitLab API"
}

async function fetchRepo(repoID: number) {
  const response = await fetch(
    `https://gitlab.com/api/v4/projects/${repoID}/repository/contributors?private_token=${process.env.GITLAB_PERSONAL_TOKEN}`
  )

  if (response.ok) {
    const data = response.json()

    return data
  }

  return null
}

async function fetchSubGroups(ogGroupID: number) {
  const response = await fetch(
    `https://gitlab.com/api/v4/groups/${ogGroupID}/descendant_groups?private_token=${process.env.GITLAB_PERSONAL_TOKEN}`
  )

  if (response.ok) {
    const data = response.json()

    return data
  }

  return null
}

async function fetchReposPerGroup(groupID: number) {
  const response = await fetch(
    `https://gitlab.com/api/v4/groups/${groupID}/projects?simple=true&per_page=50&private_token=${process.env.GITLAB_PERSONAL_TOKEN}`
  )

  if (response.ok) {
    const data = await response.json()

    return data
  }

  return null
}

async function getGroupIDs({
  calincsID,
  data,
}: {
  calincsID: number
  data: any
}) {
  const groupIDs = [calincsID]

  for (const a in data) {
    groupIDs.push(data[a].id)
  }

  return groupIDs
}

/**
 * Checks if the repository's last activity was past 6 months. If yes, then ignore it. If no, then count it.
 *
 * @param repo The repo object returned by the GitLab API
 * @returns {Boolean} A boolean to represent the validity of the repo based off its last activity date.
 */

function checkActivity(
  repo: { [key: string]: any },
  activityThreshold: number[]
) {
  //THIS SHOULD BE USED WITH THE DATE THAT THE USER INPUTS
  //AKA GET RID OF GETACTIVITYTHRESHOLD()
  const lastActivity = repo.last_activity_at
  const lastActivityArr = lastActivity.split("T", 1)[0].split("-", 3)

  for (const a in activityThreshold) {
    if (activityThreshold[a] < lastActivityArr[a]) {
      //If y/m/d is bigger than threshhold, all good.
      return true
    } else if (activityThreshold[a] > lastActivityArr[a]) {
      //If y/m/d is smaller than threshold, not good.
      return false
    }
    //If year or month is equal, go check if next step (year -> month, month -> day).
  }

  return true //If everything is equal, (threshold === lastActivity) count it as good
}

// statistics: Object
//   counts: Object
//     all: 6052
//     closed: 4717
//     opened: 1335
