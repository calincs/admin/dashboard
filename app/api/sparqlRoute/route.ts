import { NextResponse, NextRequest } from "next/server"

import { SPARQL_ENDPOINT, LINKMAP } from "@/app/lib/definitions"

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("query")

  if (query === "triplesCount") {
    const labelQuery =
      "SELECT ?s ?o WHERE {GRAPH <http://metadata.lincsproject.ca> {?s <http://purl.org/dc/terms/alternative> ?o .}} ORDER BY ?s ?o"
    const labelResponse = await fetch(SPARQL_ENDPOINT + "/sparql", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded", // Set the content type for POST
      },
      body: new URLSearchParams({ query: labelQuery }).toString(), // Convert query parameters to URL-encoded string
    })

    if (labelResponse.ok) {
      const labelData = await labelResponse.json()
      const { labels, uris } = getLabelsAndUris(labelData.results.bindings)
      const countArr = []

      for (const a in uris) {
        const countQuery = `SELECT (COUNT(*) as ?count){GRAPH <${uris[a]}> {?s ?p ?o}}`
        const countResponse = await fetch(SPARQL_ENDPOINT + "/sparql", {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/x-www-form-urlencoded", // Set the content type for POST
          },
          body: new URLSearchParams({ query: countQuery }).toString(), // Convert query parameters to URL-encoded string
        })

        if (countResponse.ok) {
          const countData = await countResponse.json()
          const { count } = getCount(countData.results.bindings)

          countArr.push(count)
        }
      }

      return NextResponse.json({
        labels: labels,
        uris: uris,
        count: countArr,
        linkMap: LINKMAP,
      })
    }

    throw "No data received from API"
  }

  throw "No query received"
}

function getLabelsAndUris(retObj: []) {
  const labels = []
  const uris = []

  for (const a in retObj) {
    const buffer = retObj[a] as {
      [key: string]: { [key: string]: string }
    }

    labels.push(buffer.o.value)
    uris.push(buffer.s.value)
  }

  return { labels: labels, uris: uris }
}

function getCount(retObj: Array<{ [key: string]: { [key: string]: string } }>) {
  const count = retObj[0].count.value

  return { count: count }
}
