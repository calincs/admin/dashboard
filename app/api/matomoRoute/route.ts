import { NextResponse, NextRequest } from "next/server"

import { SITEIDS } from "@/app/lib/definitions"

export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("query")
  const startDate = searchParams.get("start")
  const endDate = searchParams.get("end")

  if (query === "uniqueVisitors") {
    const uniqueVisitors = []
    const siteNames = []

    for (const a in SITEIDS) {
      let response = await fetch(
        `https://matomo.lincsproject.ca/index.php?module=API&method=VisitsSummary.getUniqueVisitors&idSite=${SITEIDS[a]}&period=month&date=${startDate + "," + endDate}&format=JSON&token_auth=${process.env.MATOMO_TOKEN_AUTH}`
      )

      if (response.ok) {
        const data = await response.json()

        uniqueVisitors.push(getVisitsSum(data))
      } else {
        uniqueVisitors.push(0)
      }

      response = await fetch(
        `https://matomo.lincsproject.ca?module=API&method=SitesManager.getSiteFromId&idSite=${SITEIDS[a]}&format=JSON&token_auth=${process.env.MATOMO_TOKEN_AUTH}`
      )

      if (response.ok) {
        const data = await response.json()

        siteNames.push(data.name)
      } else {
        siteNames.push("N/A")
      }
    }

    return NextResponse.json({
      siteNames: siteNames,
      uniqueVisitors: uniqueVisitors,
      siteIDs: SITEIDS,
    })
  }

  throw "No query received"
}

// function getRange() {
//   const activityThreshold = getActivityThreshold().join("-")
//   const today = new Date()
//   const todayStr = [
//     today.getFullYear(),
//     today.getMonth() + 1,
//     today.getDay(),
//   ].join("-")

//   return `${activityThreshold + "," + todayStr}`
// }

function getVisitsSum(obj: { [key: string]: number }) {
  let sum = 0
  const keys = Object.keys(obj)

  for (const a in keys) {
    sum += obj[keys[a]]
  }

  return sum
}
