import { NextResponse, NextRequest } from "next/server"
import KcAdminClient from "@keycloak/keycloak-admin-client"

/**
 * A GET request that returns the total number of users (based off number of accounts made) and recent logins.
 *
 * @URLparam query: a string representing the query. It could be either numUsers or numActiveUsers
 * @URLparam defaultStart: a string (YYYY-MM-DD) representing the start date we'd like to define as a still "active" user or "recent" login
 *
 * @param request The GET request
 * @returns An object with "result: ##" as its only property.
 */
export async function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("query")
  const defaultStart = (searchParams.get("defaultStart") as String)
    .split("-")
    .map(Number)

  const kcAdminClient = new KcAdminClient({
    baseUrl: "https://keycloak.lincsproject.ca",
    realmName: "lincs",
  })

  await kcAdminClient.auth({
    username: process.env.KEYCLOAK_USER,
    password: process.env.KEYCLOAK_PASSWORD,
    grantType: "password",
    clientId: "admin-cli",
    clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
  })

  const accessToken = kcAdminClient.accessToken

  if (query === "numUsers") {
    let data

    try {
      data = await kcAdminClient.users.count()
    } catch (error) {
      throw "No data received from Keycloak API"
    }

    return NextResponse.json({ result: data })
  } else if (query === "numActiveUsers") {
    const response = await fetch(
      `https://keycloak.lincsproject.ca/admin/realms/lincs/clients`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    )

    if (response.ok) {
      let count = 0
      const data = (await response.json()) as Array<{ [key: string]: string }>

      for (const a in data) {
        const clientUUID = data[a].id
        const userSessionResponse = await fetch(
          `https://keycloak.lincsproject.ca/admin/realms/lincs/clients/${clientUUID}/user-sessions`,
          {
            method: "GET",
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          }
        )

        if (userSessionResponse.ok) {
          const userSessionData = (await userSessionResponse.json()) as Array<{
            [key: string]: string
          }>

          for (const b in userSessionData) {
            if (
              activityCheck(Number(userSessionData[b].lastAccess), defaultStart)
            ) {
              count += 1
            }
          }
        }
      }

      return NextResponse.json({ result: count })
    }

    throw "No data received from Keycloak API"
  } else if (query === "numNewUsers") {
    let count = 0
    const response = await fetch(
      `https://keycloak.lincsproject.ca/admin/realms/lincs/users`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    )

    if (response.ok) {
      const userArr = (await response.json()) as Array<{
        [key: string]: string
      }>

      for (const userIndex in userArr) {
        if (
          activityCheck(
            Number(userArr[userIndex].createdTimestamp),
            defaultStart
          )
        ) {
          count += 1
        }
      }
    }

    return NextResponse.json({ result: count })
  }

  throw "No query received"
}

function activityCheck(unixNum: number, activityThreshold: number[]) {
  const date = new Date(unixNum)
  const lastActivityArr = [
    date.getFullYear(),
    date.getMonth() + 1,
    date.getDate(),
  ] //getMonth IS 0-BASED (Jan -> 0)

  for (const a in activityThreshold) {
    if (activityThreshold[a] < lastActivityArr[a]) {
      //If y/m/d is bigger than threshhold, all good.
      return true
    } else if (activityThreshold[a] > lastActivityArr[a]) {
      //If y/m/d is smaller than threshold, not good.
      return false
    }
    //If year or month is equal, go check if next step (year -> month, month -> day).
  }
}
