import { NextResponse, NextRequest } from "next/server"

import pubObject from "@/data/pubObject.json"
import presObject from "@/data/presObject.json"

export function GET(request: NextRequest) {
  const searchParams = request.nextUrl.searchParams
  const query = searchParams.get("query")

  if (query === "pub") {
    return NextResponse.json(pubObject)
  } else if (query === "pres") {
    return NextResponse.json(presObject)
  } else if (query === "numWorkshops") {
    let sum = 0
    const workshops = presObject.Workshops

    for (const year of Object.values(workshops)) {
      sum += year
    }

    return NextResponse.json({ result: sum })
  }

  throw "No query received"
}
