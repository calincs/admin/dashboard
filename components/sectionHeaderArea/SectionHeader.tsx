import { Link } from "@nextui-org/link"

export default function SectionHeader({
  header,
  href,
  subscript,
  description,
}: {
  header: string
  href?: string
  subscript?: string
  description?: string
}) {
  return (
    <div className="flex flex-col pb-3">
      <div className="md:flex md:flex-row">
        <Link
          href={href}
          target="_blank"
          rel="external"
          underline={href ? "hover" : "none"}
          color="foreground"
          showAnchorIcon={href ? true : false}
          anchorIcon={<AnchorIcon />}
        >
          <h1 className="text-4xl font-bold">{header}</h1>
        </Link>
        <div className="content-end">
          <p className="md:whitespace-pre-wrap italic">{subscript}</p>
        </div>
      </div>
      <p className="text-sm italic">{description}</p>
    </div>
  )
}

function AnchorIcon() {
  return (
    <svg
      fill="#000000"
      height="16px"
      width="16px"
      viewBox="0 0 36 36"
      version="1.1"
      preserveAspectRatio="xMidYMid meet"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      className="xl:hidden"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0" />
      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <g id="SVGRepo_iconCarrier">
        <title>pop-out-line</title>
        <path
          className="clr-i-outline clr-i-outline-path-1"
          d="M27,33H5a2,2,0,0,1-2-2V9A2,2,0,0,1,5,7H15V9H5V31H27V21h2V31A2,2,0,0,1,27,33Z"
        />
        <path
          className="clr-i-outline clr-i-outline-path-2"
          d="M18,3a1,1,0,0,0,0,2H29.59L15.74,18.85a1,1,0,1,0,1.41,1.41L31,6.41V18a1,1,0,0,0,2,0V3Z"
        />
        <rect x="0" y="0" width="36" height="36" fillOpacity="0" />
      </g>
    </svg>
  )
}
