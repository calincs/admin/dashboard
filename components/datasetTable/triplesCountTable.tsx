"use client"

import useSWR from "swr"
import {
  Table,
  TableHeader,
  TableBody,
  TableColumn,
  TableRow,
  TableCell,
  getKeyValue,
} from "@nextui-org/table"
import { Card, CardBody } from "@nextui-org/card"
import { Skeleton } from "@nextui-org/skeleton"
import { Link } from "@nextui-org/link"

export default function TriplesCountTable() {
  const fetcher = (response: any) => fetch(response).then((res) => res.json())
  const label = [
    { label: "LABEL", key: "label" },
    { label: "COUNT", key: "count" },
  ]
  const items = []

  const { data, error, isLoading } = useSWR(
    "/api/sparqlRoute?query=triplesCount",
    fetcher
  )

  const formatNumber = (num: number) => {
    return new Intl.NumberFormat().format(num);
  };


  if (isLoading) {
    for (let a = 0; a < 10; a++) {
      items.push({ count: <CountSkeleton />, label: <LabelSkeleton />, key: a })
    }
  } else if (error) {
    return <div>{error}</div>
  } else {
    for (const a in data.labels) {
      items.push({
        count: formatNumber(data.count[a]),
        label: (
          <Link
            href={`https://portal.lincsproject.ca/docs/explore-lod/project-datasets/${data.linkMap[data.labels[a]] ? data.linkMap[data.labels[a]] : ""}`}
            target="_blank"
            underline="hover"
            className="text-inherit"
          >
            {data.labels[a]}
          </Link>
        ),
        key: a,
      })
    }
  }

  return (
    <Card className="w-full">
      <CardBody className="text-light-teal p-0">
        <Table
          classNames={{ wrapper: "overflow-hidden" }}
          aria-label="Triples table"
          shadow="none"
        >
          <TableHeader columns={label}>
            {(column) => (
              <TableColumn key={column.key}>{column.label}</TableColumn>
            )}
          </TableHeader>
          <TableBody items={items}>
            {(item) => (
              <TableRow key={item.key}>
                {(columnKey) => (
                  <TableCell>{getKeyValue(item, columnKey)}</TableCell>
                )}
              </TableRow>
            )}
          </TableBody>
        </Table>
      </CardBody>
    </Card>
  )
}

function LabelSkeleton() {
  return (
    <Skeleton className="rounded-full">
      <div>######### ## ######</div>
    </Skeleton>
  )
}

function CountSkeleton() {
  return (
    <Skeleton className="rounded-full">
      <div>######/</div>
    </Skeleton>
  )
}
