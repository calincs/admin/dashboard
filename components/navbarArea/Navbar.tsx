import { Navbar, NavbarBrand, NavbarContent } from "@nextui-org/navbar"
import { Image } from "@nextui-org/image"
import { Link } from "@nextui-org/link"

import NavItem from "@/components/navbarArea/NavbarItem"

import GitLabButton from "./GitLabButton"

export default function NavigationBar() {
  return (
    <Navbar position="sticky" className="bg-dark-teal" maxWidth="full">
      <NavbarBrand className="justify-center lg:justify-start">
        <Link href="https://lincsproject.ca">
          <Image
            alt="LINCS Dashboard logo"
            src="/lincs-small.png"
            width={75}
            className="pl-4"
          />
          <p className={`text-large pl-4 font-bold text-white`}>
            LINCS Dashboard
          </p>
        </Link>
      </NavbarBrand>
      <NavbarContent justify="end" className="hidden lg:flex">
        <NavItem content="LINCS Portal" href="https://lincsproject.ca" />
        <GitLabButton />
      </NavbarContent>
    </Navbar>
  )
}
