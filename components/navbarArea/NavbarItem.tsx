import { NavbarItem } from "@nextui-org/navbar"
import { Link } from "@nextui-org/link"

export default function NavItem({
  content,
  href,
}: {
  content: string
  href: string
}) {
  return (
    <NavbarItem>
      <Link href={`${href}`}>
        <div className="text-white font-bold">{content}</div>
      </Link>
    </NavbarItem>
  )
}
