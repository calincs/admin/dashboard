import Link from "next/link"
import { Button } from "@nextui-org/button"
import { NavbarItem } from "@nextui-org/navbar"

import { GitLabIcon } from "@/public/icons/icons"

export default function GitLabButton() {
  return (
    <NavbarItem className="pr-4 pt-2">
      <Button
        isIconOnly
        aria-label="GitLab"
        href="https://gitlab.com/calincs"
        as={Link}
        className="bg-dark-teal"
      >
        <GitLabIcon />
      </Button>
    </NavbarItem>
  )
}
