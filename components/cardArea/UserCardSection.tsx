import CardMetrics from "@/components/cardArea/CardMetrics"
import { Card } from "@nextui-org/card"

export default function UserCardSection() {
  return (
    <div className="flex flex-col md:grid md:grid-cols-2 xl:grid-cols-4 gap-5">
      {/*Small possible issue that Card Metrics returns 'basis-1/4' which is why w/o div, it'll smushed into quarter of a quarter column*/}
      <div className="">
        <CardMetrics query="numUsers" category="keycloak" />
      </div>
      <div className="">
        <CardMetrics query="numActiveUsers" category="keycloak" />
      </div>
      <div className="">
        <CardMetrics query="numWorkshops" category="workshop" />
      </div>
      <div>
        <CardMetrics query="numNewUsers" category="keycloak" />
      </div>
    </div>
  )
}
