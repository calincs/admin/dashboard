import CardMetrics from "@/components/cardArea/CardMetrics"

export default function DevCardSection() {
  return (
    <div className="flex flex-col gap-5 pb-12 md:grid md:grid-cols-2 xl:grid-cols-4 xl:pb-5">
      <CardMetrics query="numActiveRepos" category="gitlab" />
      <CardMetrics query="numContributions" category="gitlab" />
      <CardMetrics query="numOpenedIssues" category="gitlab" />
      <CardMetrics query="numClosedIssues" category="gitlab" />
    </div>
  )
}
