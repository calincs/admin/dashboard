"use client"

import { Card, CardHeader, CardBody } from "@nextui-org/card"
import { Skeleton } from "@nextui-org/skeleton"
import { ReactElement, useContext } from "react"
import useSWR from "swr"

import {
  ActivityIcon,
  ClosedTicketIcon,
  GitIcon,
  LoginIcon,
  SchoolIcon,
  OpenTicketIcon,
  UserIcon,
  NewUserIcon,
} from "@/public/icons/icons"
import {
  cardCategories,
  cardQueries,
  getActivityThreshold,
  getActivityThresholdString,
} from "@/app/lib/definitions"
import { DateRangeFromContext } from "@/app/lib/dateRangeContext"

export default function CardMetrics({
  category,
  query,
}: {
  category: cardCategories
  query: cardQueries
}) {
  const { dateRange } = useContext(DateRangeFromContext)
  const fetcher = (response: any) => fetch(response).then((res) => res.json())
  let header, output, icon
  let description = null
  let isLoading, error, data

  header = "some buffer"
  icon = <ActivityIcon />
  output = "#####"

  if (category === "gitlab") {
    ;({ data, error, isLoading } = useSWR(
      `/api/gitlabRoute?query=${query}&start=${dateRange.start.toString()}&end=${dateRange.end.toString()}&defaultStart=${getActivityThreshold().join("-")}`,
      fetcher
    ))

    if (query === "numActiveRepos") {
      header = "Active Repositories"
      icon = <ActivityIcon />
    } else if (query === "numContributions") {
      header = "Recent Contributions"
      description = `since ${getActivityThresholdString()}`
      icon = <GitIcon />
    } else if (query === "numOpenedIssues") {
      header = "Open Tickets"
      icon = <OpenTicketIcon />
    } else if (query === "numClosedIssues") {
      header = "Closed Tickets"
      icon = <ClosedTicketIcon />
    }

    if (isLoading) {
      return (
        <GeneralCard
          skeleton
          header={header}
          description={description}
          icon={icon}
          output={output}
        />
      )
    } else if (error) {
      output = JSON.stringify(error)
    } else {
      output = data.result
    }
  } else if (category === "keycloak") {
    ;({ data, error, isLoading } = useSWR(
      `/api/keycloakRoute?query=${query}&defaultStart=${getActivityThreshold().join("-")}`,
      fetcher
    ))

    if (query === "numUsers") {
      header = "The LINCS Community"
      icon = <UserIcon />
    } else if (query === "numActiveUsers") {
      header = "Recent Logins"
      description = `since ${getActivityThresholdString()}`
      icon = <LoginIcon />
    } else if (query === "numNewUsers") {
      header = "New Users"
      description = `since ${getActivityThresholdString()}`
      icon = <NewUserIcon />
    }
  } else if (category === "workshop") {
    ;({ data, error, isLoading } = useSWR(
      `/api/pubRoute?query=${query}`,
      fetcher
    ))

    if (query === "numWorkshops") {
      header = "Workshops Held"
      icon = <SchoolIcon />
    }
  }

  if (isLoading) {
    return (
      <GeneralCard
        skeleton
        header={header}
        description={description}
        icon={icon}
        output={output}
      />
    )
  } else if (error) {
    output = error.toString()
  } else {
    output = data.result
  }

  return (
    <GeneralCard
      header={header}
      description={description}
      icon={icon}
      output={output}
    />
  )
}

function GeneralCard({
  header,
  description,
  icon,
  output,
  skeleton,
}: {
  header: string
  description: string | null
  icon: ReactElement
  output: string
  skeleton?: boolean
}) {
  let body

  if (skeleton) {
    body = (
      <div className="flex justify-start">
        <Skeleton className="rounded-full">
          <p className="text-2xl">{output}</p>
        </Skeleton>
      </div>
    )
  } else {
    body = (
      <p className="text-2xl text-left text-light-teal font-bold">{output}</p>
    )
  }

  return (
    <Card className="h-full flex">
      <CardHeader className="flex w-full justify-between pb-0">
        <div className="text-left">
          <p className="text-xl">{header}</p>
          <p className="italic" style={{ fontSize: "0.625rem" }}>
            {description}
          </p>
        </div>
        <div className="self-start">{icon}</div>
      </CardHeader>
      <CardBody className="pt-0 justify-end">{body}</CardBody>
    </Card>
  )
}
