"use client"

import { Popover, PopoverTrigger, PopoverContent } from "@nextui-org/popover"
import { Divider } from "@nextui-org/divider"
import { Card, CardBody } from "@nextui-org/card"
import { CalendarDate } from "@internationalized/date"
import { useCallback, useContext } from "react"
import {
  Dropdown,
  DropdownItem,
  DropdownTrigger,
  DropdownMenu,
} from "@nextui-org/dropdown"
import Image from "next/image"
import clsx from "clsx"

import { YEARS, MONTHS } from "@/app/lib/definitions"
import { DateRangeFromContext } from "@/app/lib/dateRangeContext"

/**
 * A date range selector icon that displays a calendar to select a range of dates that would change selected API queries within the dashboard
 * (like GitLab Development stats for example)
 *
 * @returns Popover component containing a RangeCalendar component and a DateInput component
 */
export default function DateRangeSelector() {
  const { dateRange } = useContext(DateRangeFromContext)

  return (
    <div
      className={clsx({
        "rounded-lg": true,
        "border-2": dateRange.error,
        "border-danger": dateRange.error,
      })}
    >
      <MyPopoverSection preposition="From" date={dateRange.start.toString()} />
      <MyPopoverSection preposition="To" date={dateRange.end.toString()} />
    </div>
  )
}

/**
 * An area that's clickable to show a month/year dropdown
 *
 * @param {"To" | "From"} preposition Either "To" or "From"
 * @param {string} date The date selected in string ISO 8601 format
 *
 * @returns A Popover component
 */
function MyPopoverSection({
  preposition,
  date,
}: {
  preposition: "To" | "From"
  date: string
}) {
  return (
    <Popover
      placement="right"
      showArrow={false}
      classNames={{
        content: "p-0 border-0",
        trigger: "w-full",
      }}
    >
      <PopoverTrigger>
        <Card isHoverable disableRipple isPressable shadow="none">
          <CardBody className="flex flex-row justify-between items-center text-base hover:font-bold">
            <p>
              {preposition}: {date}
            </p>
            <Image
              src="/chevronRight.png"
              alt="Chevron Right Icon"
              width={12}
              height={12}
            />
          </CardBody>
        </Card>
      </PopoverTrigger>
      <PopoverContent className="gap-4">
        <MyPopoverSelection preposition={preposition} />
      </PopoverContent>
    </Popover>
  )
}

/**
 * The area that shows the dropdown selection to select a month and a year
 *
 * @param {string} preposition To or From
 *
 * @returns A div containing a Dropdown section
 */
function MyPopoverSelection({ preposition }: { preposition: "To" | "From" }) {
  return (
    <div className="flex flex-col items-stretch" style={{ width: "230px" }}>
      <h3
        className="pl-3 py-1 bg-dark-teal text-white"
        style={{ borderTopLeftRadius: "14px", borderTopRightRadius: "14px" }}
      >
        {preposition}
      </h3>
      <div className="flex flex-row items-center gap-2 justify-between">
        <MyDropdownSelection
          title="Month"
          items={MONTHS}
          preposition={preposition}
        />
        <Divider orientation="vertical" className="h-6" />
        <MyDropdownSelection
          title="Year"
          items={YEARS}
          preposition={preposition}
        />
      </div>
    </div>
  )
}

/**
 * The dropdown area that selects the month and year
 *
 * @param {string[]} items A list of months or years
 * @param {"Month" | "Year"} title "Month" or "Year" header
 *
 * @returns A div containing a Dropdown section. The dropdown sections selects the months and year on both spectrum
 */
function MyDropdownSelection({
  items,
  title,
  preposition,
}: {
  items: string[]
  title: "Month" | "Year"
  preposition: "To" | "From"
}) {
  const { dateRange, setDateRange } = useContext(DateRangeFromContext)

  const setDate = useCallback(
    (date: string | null) => {
      const addDate = (
        targetDate: CalendarDate,
        date: string,
        title: "Month" | "Year"
      ) => {
        let newDate: CalendarDate

        if (title === "Month") {
          newDate = targetDate.set({
            month: new Date(Date.parse(date + "1, 2024")).getMonth() + 1,
          })
        } else {
          newDate = targetDate.set({ year: Number(date) })
        }

        return newDate
      }

      if (date) {
        if (preposition === "From") {
          setDateRange({
            ...dateRange,
            start: addDate(dateRange.start, date, title),
          })
        } else {
          setDateRange({
            ...dateRange,
            end: addDate(dateRange.end, date, title),
          })
        }
      }
    },
    [dateRange, preposition, title]
  )

  return (
    <div className="flex px-3 items-center h-8 w-full justify-between">
      <h3>{title}</h3>
      <Dropdown>
        <DropdownTrigger>
          <Image
            src="/chevronDown.png"
            alt="Chevron Down"
            height={6}
            width={12}
          />
        </DropdownTrigger>
        <DropdownMenu>
          {items.map((item) => {
            return (
              <DropdownItem
                key={item}
                onPress={(e) => setDate(e.target.textContent)}
              >
                {item}
              </DropdownItem>
            )
          })}
        </DropdownMenu>
      </Dropdown>
    </div>
  )
}
