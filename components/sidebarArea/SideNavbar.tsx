import { Image } from "@nextui-org/image"
import { Card, CardBody, CardHeader } from "@nextui-org/card"
import { Link } from "@nextui-org/link"
import { Divider } from "@nextui-org/divider"

import DateRangeSelector from "@/components/sidebarArea/DateRangeSelector"
export default function SideNavbar() {
  return (
    <>
      <Card>
        <CardHeader className="justify-center">
          <Link href="https://lincsproject.ca">
            <Image
              alt="LINCS Dashboard Introduction logo"
              src="/lincs.png"
              width={336}
              height={138}
              className="pt-4"
            />
          </Link>
        </CardHeader>
        <CardBody className="gap-3">
          <Divider className="hidden xl:block" />
          <p className="p-4 text-lg text-center">
            LINCS provides the tools and infrastructure to make humanities data
            more discoverable, searchable, and shareable. This dashboard
            provides a comprehensive overview of both website and publication
            metrics, offering insights into web traffic, user engagement, and
            the performance of published content. It visually represents data to
            help monitor trends and make informed decisions about content
            strategies and website optimizations.
          </p>
          <Divider />
          <DateRangeSelector />
        </CardBody>
      </Card>
    </>
  )
}
