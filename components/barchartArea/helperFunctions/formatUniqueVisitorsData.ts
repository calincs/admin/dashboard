export function getDataset(
  siteNames: Array<string>,
  uniqueVisitorsCount: Array<string>
) {
  //ret arr of obj
  let retObj: Record<string, Array<string>> = {}

  for (const a in siteNames) {
    Object.defineProperty(retObj, siteNames[a], {
      value: [],
      writable: true,
      enumerable: true,
      configurable: true,
    })
    retObj[siteNames[a]].push(uniqueVisitorsCount[a])
  }

  return retObj as { [key: string]: Array<string> }
}
