export function getLabels({
  pubObject,
}: {
  pubObject: { [key: string]: string }
}) {
  return Object.keys(pubObject)
}

//Follows an object with publication properties, each having values of the number of publications per year. Must be percise format or else it'll probably crash
export function getDatasets({
  pubObject,
}: {
  pubObject: { [key: string]: string }
}) {
  let retObj: Record<string, Array<string>> = {}
  const publications = Object.values(pubObject)
  const years = Object.keys(publications[0])

  //Maybe some typing error would occur if year don't sync up
  for (const a in years) {
    Object.defineProperty(retObj, years[a], {
      value: [],
      writable: true,
      enumerable: true,
      configurable: true,
    })

    for (const b in publications) {
      retObj[years[a]].push(Object.values(publications[b])[a])
    }
  }

  return retObj as { [key: string]: Array<string> }
}
