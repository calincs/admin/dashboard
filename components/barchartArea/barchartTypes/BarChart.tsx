import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js"
import { Bar } from "react-chartjs-2"

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend)

export default function BarChart({
  labels,
  dataset,
  barLabel,
  bgColour,
  orientation,
}: {
  labels: string[]
  dataset: number[]
  barLabel: string
  bgColour: string
  orientation: "y" | "x"
}) {
  const options = {
    indexAxis: orientation,
    elements: {
      bar: {
        borderWidth: 2,
      },
    },
    responsive: true,
    maintainAspectRatio: false,
    resizeDelay: 100,
  }
  const data = {
    labels,
    datasets: [
      {
        label: barLabel,
        data: dataset,
        backgroundColor: bgColour,
        barPercentage: 0.7,
      },
    ],
  }

  return <Bar redraw options={options} data={data} />
}
