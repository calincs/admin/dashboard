import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js"
import { Bar } from "react-chartjs-2"

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend)

export default function StackedBarChart({
  labels,
  dataset,
  barColours,
  orientation,
}: {
  labels: string[]
  dataset: { [key: string]: Array<string> }
  barColours: { [key: string]: string }
  orientation: "y" | "x"
}) {
  const options = {
    indexAxis: orientation,
    responsive: true,
    maintainAspectRatio: false,
    resizeDelay: 100,
    scales: {
      x: {
        stacked: true,
      },
      y: {
        stacked: true,
      },
    },
  }

  const data = {
    labels,
    datasets: getDataFormatting({ dataset, barColours }),
  }

  return <Bar redraw options={options} data={data} />
}

function getDataFormatting({
  dataset,
  barColours,
}: {
  dataset: { [key: string]: Array<string> }
  barColours: { [key: string]: string }
}) {
  const retObj = []

  const years = Object.keys(dataset)

  for (const a in years) {
    retObj.push({
      label: years[a],
      data: dataset[years[a]],
      backgroundColor: barColours[years[a]],
    })
  }

  return retObj
}
