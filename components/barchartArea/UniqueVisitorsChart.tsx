"use client"

import useSWR from "swr"
import { useContext } from "react"
import { Card, CardHeader, CardBody } from "@nextui-org/card"
import { Skeleton } from "@nextui-org/skeleton"

import StackedBarChart from "@/components/barchartArea/barchartTypes/StackedBarChart"
import BarChart from "@/components/barchartArea/barchartTypes/BarChart"
import { getDataset } from "@/components/barchartArea/helperFunctions/formatUniqueVisitorsData"
import {
  STACKED_MATOMO_BAR_CHART_COLOURS,
  getDateString,
} from "@/app/lib/definitions"
import { DateRangeFromContext } from "@/app/lib/dateRangeContext"

export default function UniqueVisitorsChart() {
  const { dateRange } = useContext(DateRangeFromContext)
  let startRange, endRange
  if (dateRange.error) {
    startRange = dateRange.defaultStart.toString()
    endRange = dateRange.defaultEnd.toString()
  } else {
    startRange = dateRange.start.toString()
    endRange = dateRange.end.toString()
  }
  const rangeStr = `   from ${getDateString(startRange.split("-").map(Number))} to ${getDateString(endRange.split("-").map(Number))} `
  const fetcher = (response: any) => fetch(response).then((res) => res.json())

  const { isLoading, error, data } = useSWR(
    `/api/matomoRoute?query=uniqueVisitors&start=${startRange}&end=${endRange}`,
    fetcher
  )

  if (isLoading) {
    return <MySkeleton />
  } else if (error) {
    return <div>{error}</div>
  }

  const labels = ["Total Unique Visitors"]
  const dataset = getDataset(data.siteNames, data.uniqueVisitors)
  const siteNames = data.siteNames as Array<string>
  const uniqueVisitors = data.uniqueVisitors as Array<number>

  return (
    <>
      <div className="w-full hidden md:block xl:col-span-2">
        <Card className="h-60">
          <CardHeader className="text-xl">
            <div className="flex flex-col xl:flex-row pb-3">
              <h1>LINCS Tool usage: Total number of unique visits</h1>
              <div className="content-end">
                <p className="xl:whitespace-pre-wrap italic text-sm">
                  {rangeStr}
                </p>
              </div>
            </div>
          </CardHeader>
          <CardBody>
            <StackedBarChart
              labels={labels}
              dataset={dataset}
              barColours={STACKED_MATOMO_BAR_CHART_COLOURS}
              orientation="y"
            />
          </CardBody>
        </Card>
      </div>
      <div className="w-full block md:hidden">
        <Card className="h-128">
          <CardHeader className="text-xl">
            <div className="flex flex-col xl:flex-row pb-3">
              <h1>LINCS Tool usage: Total number of unique visits</h1>
              <div className="content-end">
                <p className="xl:whitespace-pre-wrap italic text-sm">
                  {rangeStr}
                </p>
              </div>
            </div>
          </CardHeader>
          <CardBody>
            <BarChart
              labels={siteNames}
              dataset={uniqueVisitors}
              barLabel="Tools"
              bgColour="rgb(75, 192, 192)"
              orientation="x"
            />
          </CardBody>
        </Card>
      </div>
    </>
  )
}

function MySkeleton() {
  return (
    <div>
      <div className="w-full hidden md:block xl:col-span-2">
        <Card className="h-60">
          <CardHeader className="text-xl">
            <div className="flex flex-col xl:flex-row pb-3">
              <Skeleton className="rounded-xl">
                <h1>LINCS Tool usage: Total number of unique visits</h1>
              </Skeleton>
            </div>
          </CardHeader>
          <CardBody>
            <div className="w-full h-full flex items-center gap-5">
              <Skeleton className="w-1/6 h-1/6 rounded-xl " />
              <Skeleton className="w-full h-1/2 rounded-xl" />
            </div>
          </CardBody>
        </Card>
      </div>
      <div className="w-full block md:hidden xl:col-span-2">
        <Card className="h-96">
          <CardHeader className="text-xl">
            <div className="flex flex-col xl:flex-row pb-3">
              <Skeleton className="rounded-xl">
                <h1>LINCS Tool usage: Total number of unique visits</h1>
              </Skeleton>
            </div>
          </CardHeader>
          <CardBody>
            <div>
              <div className="flex flex-row h-3 gap-5 pl-16 pr-16 justify-items-center">
                <Skeleton className="h-full basis-1/5 rounded-xl" />
                <Skeleton className="h-full basis-1/5 rounded-xl" />
                <Skeleton className="h-full basis-1/5 rounded-xl" />
                <Skeleton className="h-full basis-1/5 rounded-xl" />
                <Skeleton className="h-full basis-1/5 rounded-xl" />
              </div>
              <div className="h-60 flex flex-row gap-5 p-10 pb-3 items-end">
                <Skeleton className="h-full basis-1/5 rounded-xl" />
                <Skeleton className="h-1/3 basis-1/5 rounded-xl" />
                <Skeleton className="h-1/5 basis-1/5 rounded-xl" />
                <Skeleton className="h-5/6 basis-1/5 rounded-xl" />
                <Skeleton className="h-1/2 basis-1/5 rounded-xl" />
              </div>
            </div>
          </CardBody>
        </Card>
      </div>
    </div>
  )
}
