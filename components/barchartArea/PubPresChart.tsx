"use client"

import useSWR from "swr"
import { Card, CardHeader, CardBody } from "@nextui-org/card"
import { Skeleton } from "@nextui-org/skeleton"

import StackedBarChart from "@/components/barchartArea/barchartTypes/StackedBarChart"
import { STACKED_PUBPRES_BAR_CHART_COLOURS } from "@/app/lib/definitions"
import {
  getDatasets,
  getLabels,
} from "@/components/barchartArea/helperFunctions/formatPubPresData"

export default function PubPresChart({
  type,
}: {
  type: "publication" | "presentation"
}) {
  const fetcher = (response: any) => fetch(response).then((res) => res.json())
  let isLoading, error, data

  if (type === "publication") {
    ;({ isLoading, error, data } = useSWR("/api/pubRoute?query=pub", fetcher))
  } else if (type === "presentation") {
    ;({ isLoading, error, data } = useSWR("/api/pubRoute?query=pres", fetcher))
  }

  if (isLoading) {
    return <GeneralCard skeleton />
  } else if (error) {
    return <div>{error}</div>
  }

  const labels = getLabels({ pubObject: data as { [key: string]: string } })
  const dataset = getDatasets({ pubObject: data as { [key: string]: string } })

  return <GeneralCard labels={labels} dataset={dataset} type={type} />
}

function GeneralCard({
  labels,
  dataset,
  skeleton,
  type,
}: {
  skeleton?: boolean
  dataset?: {
    [key: string]: string[]
  }
  labels?: string[]
  type?: string
}) {
  let body, head

  if (skeleton) {
    head = (
      <Skeleton className="rounded-full">
        <div>Number of Presentations</div>
      </Skeleton>
    )
    body = (
      <div>
        <div className="flex flex-row h-3 gap-5 pl-16 pr-16 justify-items-center">
          <Skeleton className="h-full basis-1/5 rounded-xl" />
          <Skeleton className="h-full basis-1/5 rounded-xl" />
          <Skeleton className="h-full basis-1/5 rounded-xl" />
          <Skeleton className="h-full basis-1/5 rounded-xl" />
          <Skeleton className="h-full basis-1/5 rounded-xl" />
        </div>
        <div className="h-60 flex flex-row gap-5 p-10 pb-3 items-end">
          <Skeleton className="h-full basis-1/5 rounded-xl" />
          <Skeleton className="h-1/3 basis-1/5 rounded-xl" />
          <Skeleton className="h-1/5 basis-1/5 rounded-xl" />
          <Skeleton className="h-5/6 basis-1/5 rounded-xl" />
          <Skeleton className="h-1/2 basis-1/5 rounded-xl" />
        </div>
      </div>
    )
  } else if (labels && dataset && type) {
    head =
      type === "publication"
        ? "Number of Publications"
        : "Number of Presentations"
    body = (
      <StackedBarChart
        labels={labels}
        dataset={dataset}
        barColours={STACKED_PUBPRES_BAR_CHART_COLOURS}
        orientation="x"
      />
    )
  }

  return (
    <Card className="h-96">
      <CardHeader className="justify-center text-xl">{head}</CardHeader>
      <CardBody>{body}</CardBody>
    </Card>
  )
}
