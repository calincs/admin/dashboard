# LINCS Dashboard

The LINCS Dashboard is a Node project that uses the React framework. It contains an overview of the status of trackable elements of the LINCS project that include:

- Web site stats (visits)
- Number of users registered
- LINCS users logged into tools
- Number of datasets
- Number of triples
- Size of triplestore
- Dev stats (commits, issues created/closed)
- Infrastructure stats (cpu/ram/storage)
- Youtube stats
- Qualtrix stats
- Publications (link to Portal)

Some data elements are manually maintained while others are retrieved in realtime from the related services.

## Development

It is strongly recommended to maintain this project with `VSCode` and use the official `ESLint` extension from _Microsoft_. The `Prettier - Code Formatter` from _prettier.io_ is also good to add.

### Environment

You need to create a local `.env` file that contain these variables:

```bash
MATOMO_TOKEN_AUTH="matomo_auth_token"
GITLAB_PERSONAL_TOKEN="gitlab_token"
KEYCLOAK_USER=dashboard
KEYCLOAK_PASSWORD="the_password"
KEYCLOAK_CLIENT_SECRET="the_secret"
SESSION_SECRET="a_random_string - Generate with openssl rand -base64 32"
```

### Run with Docker

```bash
docker compose up
```

This should make the service accessible at `http://localhost:3000`

### Run locally with Node

If you want to host the API locally without using Docker, you must have Node.js v20+ and npm installed (instructions for how to download these dependencies can be found [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)).

To see if you already have Node.js and npm installed run the following commands:

```bash
npm version
```

To run the development server:

```node
npm install
npm run dev
```

Use `npm run clean` to delete build artifacts, `npm run test` to run unit tests, and `npm run lint` to run eslint checks.
