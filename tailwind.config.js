import { nextui } from "@nextui-org/theme"

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        black: "#000000",
        "light-teal": "#107386",
        gray: "#7E97A9",
        white: "#FFFFFF",
        "dark-teal": "#122E45",
      },
      fontFamily: {
        sans: ["var(--font-sans)"],
        mono: ["var(--font-geist-mono)"],
      },
      height: {
        128: "32rem",
      },
    },
  },
  darkMode: "class",
  plugins: [
    nextui({
      prefix: "dashboard",
      layout: {
        dividerWeight: "1px", // h-divider the default height applied to the divider component
        disabledOpacity: 0.5, // this value is applied as opacity-[value] when the component is disabled
      },
      theme: {
        light: {
          colors: {
            foreground: "#333333",
            background: "#FFFFFF",
            primary: {
              700: "#2b838f",
              900: "#205b63",
              default: "#f0f4f8",
            },
            secondary: {
              default: "#FFFFFF",
            },
          },
        },
        dark: {},
      },
    }),
  ],
}
